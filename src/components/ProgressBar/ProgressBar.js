/* eslint-disable no-unused-vars */
import React from 'react';
import styled from 'styled-components';

import { COLORS } from '../../constants';
import VisuallyHidden from '../VisuallyHidden';

const SIZES = {
  large:{
    "--padding": "4px",
    "--border-radius": "8px",
    "--height": "24px",
    "--border-radius-internal": "5px",
  },
  medium: {
   "--padding": "0px",
    "--border-radius": "4px",
    "--height": "12px",
    "--border-radius-internal": "4px",
  },
  small: {
    "--padding": "0px",
    "--border-radius": "4px",
    "--height": "8px",
    "--border-radius-internal": "4px",
  }
}

const ProgressBar = ({ value, size }) => {
  const styles  = SIZES[size];
  
  if(!styles){
    throw new Error('Unknown size passed top ProgressBar');
  }

  return (
    <Wrapper 
        role="progressbar" 
        aria-valuenow={value} 
        aria-valuemin="0" 
        aria-valuemax="100" 
        
        style={styles}
    >
      <BarWrapper>
        <VisuallyHidden>{value}</VisuallyHidden>
        <BarBase style={{ '--width': value + '%'}} />
      </BarWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  background-color: ${COLORS.transparentGray15};
  box-shadow: 0px 2px 4px ${COLORS.transparentGray35} inset;

  border-radius: var(--border-radius);
  padding: var(--padding);
`

const BarWrapper = styled.div`
  border-radius: var(--border-radius-internal);
  overflow: hidden;
`

const BarBase = styled.div`
  width: var(--width);
  height: var(--height);
  background-color: ${COLORS.primary};
`


export default ProgressBar;
